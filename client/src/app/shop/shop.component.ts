import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IBrand } from '../models/Brand';
import { IProducts } from '../models/Products';
import { IType } from '../models/ProductType';
import { ShopService } from './shop.service';
import {ShopParams} from '../models/ShopParams'


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  @ViewChild('search') searchTerm : ElementRef
  products : IProducts[];
  brands : IBrand[];
  types : IType[];
  shopParams = new ShopParams();
  totalCount : number;

  sortOptions = 
  [
    {name : 'Alphabetical', value : 'name'},
    {name : 'Price: Low to High', value : 'priceAsc'},
    {name : 'Price: High to Low', value : 'priceDesc'}
  ]

  constructor(private shopService:ShopService) 
  {
    
  }

  ngOnInit( ): void {
    this.getBrands();
    this.getTypes();
    this.getProducts();
  }

  getProducts()
  {
    this.shopService.getProducts(this.shopParams)
    .subscribe(response => {
      this.products = response.data;
      this.shopParams.pageIndex = response.pageIndex;
      this.shopParams.pageSize = response.pageSize;
      this.totalCount = response.count
    }, error => console.log(error));
  }

  getTypes()
  {
    this.shopService.getTypes().subscribe(response => {
      this.types = [{id : 0, name : 'All'}, ...response];;
    }, error => console.log(error));
  }

  getBrands()
  {
    this.shopService.getBrands().subscribe(response => {
      this.brands = [{id : 0, name : 'All'}, ...response];
    }, error => console.log(error));
  }

  onBrandSelected(brandId : number)
  { 
    this.shopParams.brandId = brandId;
    this.shopParams.pageIndex = 1
    this.getProducts();
  }

  onTypeSelected(typeId : number)
  { 
    this.shopParams.typeId = typeId;
    this.shopParams.pageIndex = 1
    this.getProducts();
  }

  onSortSelected(sort : string)
  {
    this.shopParams.sortItemSelected = sort;
    this.getProducts();
  }

  onPageChanged(event : any)
  {
    if(this.shopParams.pageIndex !== event)
    {
      this.shopParams.pageIndex = event;
      this.getProducts();
    }
  }

  onSearch()
  {
    this.shopParams.searchQuery = this.searchTerm.nativeElement.value;
    this.shopParams.pageIndex = 1
    this.getProducts();
  }

  onClear()
  {
    this.searchTerm.nativeElement.value = "";
    this.shopParams = new ShopParams();
    this.getProducts();
  }

}
