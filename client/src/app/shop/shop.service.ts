import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBrand } from '../models/Brand';
import { IPagination } from '../models/Pagination';
import { IType } from '../models/ProductType';
import {map} from 'rxjs/operators';
import { ShopParams } from '../models/ShopParams';

@Injectable({
  providedIn: 'root'
})
export class ShopService 
{
  baseUrl = 'https://localhost:5001/api/'

  constructor(private http:HttpClient) { }

  getProducts(shopParams : ShopParams)
  {
    let params = new HttpParams();

    if(shopParams.brandId !== 0)
    {
      params = params.append('brandId', shopParams.brandId.toString());
    }
    if(shopParams.typeId !== 0)
    {
      params = params.append('typeId', shopParams.typeId.toString());
    }
    if(shopParams.searchQuery)
    {
      params = params.append('search', shopParams.searchQuery);
    }
    
    params = params.append('sort', shopParams.sortItemSelected);

    params = params.append('pageIndex', shopParams.pageIndex.toString());

    params = params.append('pageSize', shopParams.pageSize.toString());
    

    return this.http.get<IPagination>(this.baseUrl + 'products', {observe : 'response', params})
            .pipe
            (
              map(response =>
                {
                  return response.body
                })
            ) 
  }

  getBrands()
  {
      return this.http.get<IBrand[]>(this.baseUrl + 'products/brands')
  }

  getTypes()
  {
      return this.http.get<IType[]>(this.baseUrl + 'products/types')
  }
}