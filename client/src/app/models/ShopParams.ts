export class ShopParams
{
  brandId = 0;
  typeId = 0;
  sortItemSelected = 'name';
  pageIndex = 1;
  pageSize = 6;
  searchQuery = "";
}